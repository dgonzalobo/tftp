package es.dgonzalobo;
import java.io.*;
import java.util.*;
import java.net.*;

class servTFTP extends frame {
	private DatagramSocket ds;

	public servTFTP(){
		try {
			ds = new DatagramSocket(ServerPort);
		} 
		catch (SocketException e) {
			System.err.println("ERROR || constructor (servTFTP)");
		} 
	}

	public static void main(String[] args){
		try{
			servTFTP server=new servTFTP();

			System.out.println("\t****************************************************");
			System.out.println("\t                   Servidor TFTP");
			System.out.println("\t                   (PUERTO:"+ServerPort+")");
			System.out.println("\t****************************************************\n");

			while(true)
				server.listen();
		}
		catch(Exception e){
			System.err.println("ERROR || main (servTFTP) "+e);
		}

	}

	public void listen(){
		try{
			DatagramPacket dp = new DatagramPacket(new byte[512], 512);
			ds.receive(dp);
			System.out.println("Connection detected in Master");
			new Thread(new ThreadServTFTP(dp)).start();
		}
		catch(IOException e){
			System.err.println("ERROR || listen (servTFTP)");
		}
	}
}

class ThreadServTFTP extends frame implements Runnable {

	private LinkedList <DatagramPacket> listdp; // lista de paquetes recibidos
	private PipedOutputStream o; 
	private PipedInputStream i;
	private Timer t; // referencia al gestor de timers
	private Line l; // referencia al gestor de lineas
	private FileInputStream fileReader;
	private String file, name; // nombre del fichero
	private int event, state = espera, leido; // variables de evento y estado (inicialmente en espera)

	public ThreadServTFTP(DatagramPacket dp) {	
		try{
			remoteTID = dp.getPort();
			name="Cliente/"+remoteTID;
			a = dp.getAddress();
			rec = new DatagramPacket(dp.getData(), dp.getData().length);
			o = new PipedOutputStream();
			i = new PipedInputStream(o);
			sock = new DatagramSocket();
			listdp = new LinkedList<DatagramPacket>();
			t = new Timer(o);
			l = new Line(o, listdp, sock);
			seqnum = 1;
			file = file(rec);
			fileReader = new FileInputStream(file);
		}
		catch(Exception e){
			System.err.println("ERROR || constructor (ThreadServTFTP)");
		}
	}

	public void run() {
		System.out.println("Slave take the control of conection "+name);
		System.out.println("\tPackege Recieve from "+name+"||Type: "+code(rec));
		requestRRQ();
	}

	public void requestRRQ() {
		try {
			leido = sendDATA();
			t.startTimers(); // inicializacion de los timers
			state = recibiendo; // cambio de estado a recibiendo

			while(state != espera) { // bucle de espera de eventos
				event = i.read();				
				switch (event) {				
				case frame: // procesa la trama
					synchronized(listdp) {							
						rec = listdp.removeFirst();
					}						
					if(code(rec) == ACK) {
						System.out.println("\tPackege Recieve from "+name+"||Type: ACK");
						if((seqnum(rec)+1) == seqnum){ // compara el numero de secuencia
							if(state == acabando) {
								state = espera; // cambio de estado a espera 
								t.stopTimers(); // parada de los timers
							}
							else {
								leido = sendDATA(); 			  
								t.startTimers(); // inicializacion de los timers
								if(leido < 512) // cuando se trate de la ultima trama
									state = acabando; // cambio de estado a acabando
							}	
						}
					}
					if(code(rec) == ERROR) {
						System.out.println("\tPackege Recieve from "+name+"||Type: ERROR");
						state = espera; 
						t.stopTimers();
					}
					break;

				case close: //retorna a estado inicial
					state=espera;
					break;

				case tout: // retransmite
					if(state == recibiendo)
						t.startTout();
					break;

				default:
					break;
				}
			}
		}
		catch(Exception e) { 
			System.err.println("ERROR || sendData (ThreadServTFTP) " +e); 
		}
	}

	int sendDATA() { 
		try {
			int len; 
			byte b[] = new byte[516];
			len = fileReader.read(b, 4, 512); 
			SS(sock, DATA(b, (len+4)));
			this.seqnum++;
			System.out.println("\tPackage send to "+name);
			return len;
		} 
		catch(Exception e) { 
			System.err.println("ERROR || sendDATA (ThreadServTFTP) "+e); 
			return -1;
		}
	}

}

