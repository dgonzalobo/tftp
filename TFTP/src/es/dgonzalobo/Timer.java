package es.dgonzalobo;

import java.io.*;

public class Timer implements Runnable, cons {
	static final int plazo=2, fin=4;
	int[] timer = {0,0} ; // array con contadores de los dos timers 
	PipedOutputStream o; // stream para envío de timeouts

	public Timer (PipedOutputStream ot) { 
		o = ot;
		Thread t = new Thread(this); 
		t.start(); 
	} //Timer constructor

	synchronized void startTout () { 
		timer[tout] = plazo; 
	} 
	synchronized void stopTout () { 
		timer[tout] = 0; 
	}
	synchronized void startTimers (){
		timer[tout]=plazo;
		timer[close]=plazo*fin;
	}
	synchronized void stopTimers () { 
		timer[tout] = 0;
		timer[close] = 0; 
	}
	public void run () {
		try {
			while (true) { 
				Thread.sleep (1000); 
				synchronized (this) {
					for(int i=0;i<timer.length;i++){
						if (timer[i] > 0){
							if (--timer[i] == 0) { // decrementa timer[i] y compara 
								o.write((byte)i); 
								o.flush(); // envía timeout
							}
						}
					} 
				}
			}
		} 
		catch (IOException e) {
			System.out.println("Timer: " + e);
		} 
		catch (InterruptedException e) {
			System.out.println("Timer"+e); 
		}
	}
}