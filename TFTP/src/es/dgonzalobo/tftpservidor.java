package es.dgonzalobo;

import java.io.*;
import java.net.*;

public class tftpservidor extends frame implements Runnable { 
	FileInputStream i;

	public tftpservidor() {
		Thread ts = new Thread(this);
		ts.start();
	}

	public int sendDATA () throws IOException { 
		int len; 
		byte b[] = new byte[516]; 
		len = i.read(b, 4, 512); 
		SS(auxsock, DATA(b, (len+4))); 
		this.seqnum++;
		return len;
	} // envía tramas de datos del fichero

	public void run() { 
		try {
			seqnum = 1; // inicializar número de secuencia 
			rec= new DatagramPacket(new byte[516], 516);
			sock = new DatagramSocket(ServerPort); // socket para RRQ 
			auxsock = new DatagramSocket(); // socket para resto de com. 
			i = new FileInputStream(file(rec));
			sock.receive(rec);     // espera primer RRQ
			remoteTID=rec.getPort(); 
			a=rec.getAddress(); 
			sock.receive(rec); // espera siguiente retransmision de RRQ
			int l = sendDATA(); // envía datos
			auxsock.receive(rec);  // espera ACK ( no envia DATA)
			auxsock.receive(rec);  // espera siguiente retransmision de ACK
			l=sendDATA();         // envía datos
			auxsock.receive(rec);  // espera ACK
			l = sendDATA();        // envía datos
			auxsock.receive(rec);  // espera ACK
			l = sendDATA();        // envia datos
		}
		catch (Exception e) {
			System.err.println("Error en el run. "+e);
		}
		}
	}
