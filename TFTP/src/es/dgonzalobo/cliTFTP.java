package es.dgonzalobo;

import java.io.*;
import java.net.*;
import java.util.*;

public class cliTFTP extends frame implements Runnable, cons {
	private static cliTFTP cliente;
	private String file;// parametros de activacion
	private FileOutputStream o;
	private Timer t; // referencia al gestor de timers
	private static LinkedList<DatagramPacket>listrec; // lista de paquetes recibidos
	private int event, state = espera; // variables de evento y estado (inicialmente en espera)
	private static PipedOutputStream pOUT;
	private PipedInputStream i;
	private static DatagramSocket sock;

	public cliTFTP(String h, String f){
		try{
			pOUT = new PipedOutputStream();
			i = new PipedInputStream(pOUT);
			sock = new DatagramSocket();
			t=new Timer(pOUT);
			listrec = new LinkedList<DatagramPacket>();
			file = f;
			o = new FileOutputStream("copiaencliente_" + f);
			a = InetAddress.getByName(h);
		} 
		catch(Exception e) { 
			System.err.println("ERROR || contructor (cliTFTP)"+e);
		}
	}


	public static void main(String[]args){

		System.out.println("\t****************************************************");
		System.out.println("\t                   Cliente TFTP");
		System.out.println("\t****************************************************\n");

		if(args.length==3 && args[1].equalsIgnoreCase("get"))
			cliente=new cliTFTP(args[0],args[2]);
		else{
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			try{
				System.out.println("Indique el nombre del archivo que desea subir: ");
				String fileName=br.readLine();
				cliente=new cliTFTP("localhost",fileName);
			}
			catch(IOException e){
				System.err.println("ERROR|| run (ThreadClienteTFTP) "+e);
			}
		}
		new Line(pOUT, listrec, sock); 
		cliente.run();
	}

	public void run() {
		try {
			remoteTID = ServerPort; // primera transicion
			SS(sock, (sent = RRQ(file, "netascii")));
			System.out.println("\tPackage RRQ send to Server");
			t.startTimers(); // inicializacion de los timers
			state = recibiendo; // cambio de estado a recibiendo

			while(state != espera) { // bucle de espera de eventos
				event = i.read();				
				switch(event){					
				case frame:  // procesa la trama					
					synchronized(listrec) {							
						rec = listrec.removeFirst();
					}
					if(firstDATAframe()) // guarda nuevo TID remoto				
						remoteTID=rec.getPort();							

					if(rec.getPort() != remoteTID) { // la trama no llega del servidor
						SS(sock, ERROR(0, "wrong TID"));
						System.out.println("\tPackage ERROR send to Server");
						break;
					}	
					if(code(rec) == DATA) {
						System.out.println("\tPackege Recieve from Server ||Type: DATA");
						if((seqnum(rec)) != seqnum) { // trama de datos nueva						
							seqnum = seqnum(rec); 
							t.startTimers(); 
							o.write(dat(rec));
						}							
						SS(sock, (sent = ACK())); // envia (re)asentimiento
						System.out.println("\tPackage ACK send to Server");
						if(state == recibiendo)							
							t.startTout();
						if(rec.getLength() < 512) { // ultima trama de datos
							state = acabando;
							t.stopTout();
						}
					} 
					if(code(rec) == ERROR) {
						System.out.println("\tPackege Recieve from Server ||Type: ERROR");
						state = espera;
						t.stopTimers();
					}
					break;

				case close: // retorna a estado inicial
					state=espera;
					break;

				case tout: // retransmite
					if(state == recibiendo) { 
						System.out.println("\tSend Again a Package to Server");
						SS(sock, sent);
						t.startTout();
					}
					break;

				default: 
					break;
				}
			}
			o.close();
			System.out.println("\tDOWNLOAD COMPLETE");
		} 
		catch(IOException e) { 
			System.err.println("tftpClienteHilo exception: " + e);
		}
	}

}